// Saves options to chrome.storage
function save_options() {
  var host1_text = document.getElementById('host1-text').value;
  var host2_text = document.getElementById('host2-text').value;
  var host3_text = document.getElementById('host3-text').value;
  
  var host1_extra_str = document.getElementById('host1-extra-str').value;
  var host2_extra_str = document.getElementById('host2-extra-str').value;
  var host3_extra_str = document.getElementById('host3-extra-str').value;
  
  var host1_extra_option_a = document.getElementById('host1-extra-option-a').checked;
  var host2_extra_option_a = document.getElementById('host2-extra-option-a').checked;
  var host3_extra_option_a = document.getElementById('host3-extra-option-a').checked;
  
  var host1_extra_option_p = document.getElementById('host1-extra-option-p').checked;
  var host2_extra_option_p = document.getElementById('host2-extra-option-p').checked;
  var host3_extra_option_p = document.getElementById('host3-extra-option-p').checked;
  
  var host1_port_author = document.getElementById('host1-port-author').value;  
  var host2_port_author = document.getElementById('host2-port-author').value;  
  var host3_port_author = document.getElementById('host3-port-author').value;  
  
  var host1_port_publish = document.getElementById('host1-port-publish').value;  
  var host2_port_publish = document.getElementById('host2-port-publish').value;  
  var host3_port_publish = document.getElementById('host3-port-publish').value;  

  var host1_option = document.getElementById('host1-option').checked;
  var host2_option = document.getElementById('host2-option').checked;
  var host3_option = document.getElementById('host3-option').checked;
  
  chrome.storage.sync.set({  
  
  'host1_text': host1_text,
  'host2_text': host2_text,
  'host3_text': host3_text,  
  'host1_port_author': host1_port_author,
  'host2_port_author': host2_port_author,
  'host3_port_author': host3_port_author,  
  'host1_port_publish': host1_port_publish,
  'host2_port_publish': host2_port_publish,
  'host3_port_publish': host3_port_publish,
  'host1_option': host1_option,
  'host2_option': host2_option,
  'host3_option': host3_option,
  'host1_extra_str': host1_extra_str,
  'host2_extra_str': host2_extra_str,
  'host3_extra_str': host3_extra_str,
  'host1_extra_option_a' : host1_extra_option_a,
  'host2_extra_option_a' : host2_extra_option_a,
  'host3_extra_option_a' : host3_extra_option_a,
  'host1_extra_option_p' : host1_extra_option_p,
  'host2_extra_option_p' : host2_extra_option_p,
  'host3_extra_option_p' : host3_extra_option_p

  
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}


function restore_options() {

 
  chrome.storage.sync.get({    
  
  host1_text: '',
  host2_text: '',
  host3_text: '',  
  host1_port_author: '',
  host1_port_publish: '',  
  host2_port_author: '',
  host2_port_publish: '',
  host3_port_author: '',
  host3_port_publish: '',
  host1_option: '',
  host2_option: '',
  host3_option: '',
  host1_extra_option_a: '',
  host2_extra_option_a: '',  
  host3_extra_option_a: '',  
  host1_extra_option_p: '',  
  host2_extra_option_p: '',  
  host3_extra_option_p: '',  
  host1_extra_str: '',
  host2_extra_str: '',
  host3_extra_str: ''
  
  
  
  }, function(items) {
  
    document.getElementById('host1-text').value = items.host1_text;
    document.getElementById('host2-text').value = items.host2_text;
	document.getElementById('host3-text').value = items.host3_text;
	document.getElementById('host1-option').checked = items.host1_option;
	document.getElementById('host1-port-author').value = items.host1_port_author;
	document.getElementById('host1-port-publish').value = items.host1_port_publish;	
	document.getElementById('host2-option').checked = items.host2_option;
	document.getElementById('host2-port-author').value = items.host2_port_author;
	document.getElementById('host2-port-publish').value = items.host2_port_publish;
	document.getElementById('host3-option').checked = items.host3_option;
	document.getElementById('host3-port-author').value = items.host3_port_author;
	document.getElementById('host3-port-publish').value = items.host3_port_publish;	
	
	document.getElementById('host1-port-author').disabled = !items.host1_option;
	document.getElementById('host1-port-publish').disabled = !items.host1_option;

    document.getElementById('host2-port-author').disabled = !items.host2_option;
	document.getElementById('host2-port-publish').disabled = !items.host2_option;	

    document.getElementById('host3-port-author').disabled = !items.host3_option;
	document.getElementById('host3-port-publish').disabled = !items.host3_option;
	
	document.getElementById('host1-extra-str').value = items.host1_extra_str;
	document.getElementById('host2-extra-str').value = items.host2_extra_str;
	document.getElementById('host3-extra-str').value = items.host3_extra_str;
	
	document.getElementById('host1-extra-str').disabled = items.host1_option;
	document.getElementById('host2-extra-str').disabled = items.host2_option;
	document.getElementById('host3-extra-str').disabled	= items.host3_option;
  
	document.getElementById('host1-extra-option-a').checked = items.host1_extra_option_a;
	document.getElementById('host2-extra-option-a').checked = items.host2_extra_option_a;
	document.getElementById('host3-extra-option-a').checked = items.host3_extra_option_a;
  
	document.getElementById('host1-extra-option-p').checked = items.host1_extra_option_p;
	document.getElementById('host2-extra-option-p').checked = items.host2_extra_option_p;
	document.getElementById('host3-extra-option-p').checked = items.host3_extra_option_p;
	
	document.getElementById('host1-extra-option-a').disabled = items.host1_option;
	document.getElementById('host2-extra-option-a').disabled = items.host2_option;
	document.getElementById('host3-extra-option-a').disabled = items.host3_option;
	
	document.getElementById('host1-extra-option-p').disabled = items.host1_option;
	document.getElementById('host2-extra-option-p').disabled = items.host2_option;
	document.getElementById('host3-extra-option-p').disabled = items.host3_option;	
	
	
  });
}


function restore_to_default() {

 
  chrome.storage.sync.get({    
  
  host1_text: 'kogo.cit.com.br',
  host2_text: 'localhost',
  host3_text: '',  
  host1_port_author: '',
  host1_port_publish: '',  
  host2_port_author: '4502',
  host2_port_publish: '4503',
  host3_port_author: '',
  host3_port_publish: '',
  host1_option: false,
  host2_option: true,
  host3_option: '',
  host1_extra_option_a: true,
  host2_extra_option_a: '',  
  host3_extra_option_a: '',  
  host1_extra_option_p: '',  
  host2_extra_option_p: '',  
  host3_extra_option_p: '',  
  host1_extra_str: 'author.',
  host2_extra_str: '',
  host3_extra_str: ''  
  
  }, function(items) {
  
    document.getElementById('host1-text').value = items.host1_text;
    document.getElementById('host2-text').value = items.host2_text;
	document.getElementById('host3-text').value = items.host3_text;
	document.getElementById('host1-option').checked = items.host1_option;
	document.getElementById('host1-port-author').value = items.host1_port_author;
	document.getElementById('host1-port-publish').value = items.host1_port_publish;	
	document.getElementById('host2-option').checked = items.host2_option;
	document.getElementById('host2-port-author').value = items.host2_port_author;
	document.getElementById('host2-port-publish').value = items.host2_port_publish;
	document.getElementById('host3-option').checked = items.host3_option;
	document.getElementById('host3-port-author').value = items.host3_port_author;
	document.getElementById('host3-port-publish').value = items.host3_port_publish;	

	document.getElementById('host1-extra-str').value = items.host1_extra_str;
	document.getElementById('host2-extra-str').value = items.host2_extra_str;
	document.getElementById('host3-extra-str').value = items.host3_extra_str;	
	
	document.getElementById('host1-extra-str').disabled = items.host1_option;
	document.getElementById('host2-extra-str').disabled = items.host2_option;
	document.getElementById('host3-extra-str').disabled	= items.host3_option;	
	
	document.getElementById('host1-extra-option-a').checked = items.host1_extra_option_a;
	document.getElementById('host2-extra-option-a').checked = items.host2_extra_option_a;
	document.getElementById('host3-extra-option-a').checked = items.host3_extra_option_a;
  
	document.getElementById('host1-extra-option-p').checked = items.host1_extra_option_p;
	document.getElementById('host2-extra-option-p').checked = items.host2_extra_option_p;
	document.getElementById('host3-extra-option-p').checked = items.host3_extra_option_p;
	
	document.getElementById('host1-extra-option-a').disabled = items.host1_option;
	document.getElementById('host2-extra-option-a').disabled = items.host2_option;
	document.getElementById('host3-extra-option-a').disabled = items.host3_option;
	
	document.getElementById('host1-extra-option-p').disabled = items.host1_option;
	document.getElementById('host2-extra-option-p').disabled = items.host2_option;
	document.getElementById('host3-extra-option-p').disabled = items.host3_option;		
	
	
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);
	
document.getElementById('restoreDefault').addEventListener('click',
    restore_to_default);	
	
	
	
document.getElementById('host1-option').onchange = function() {
    document.getElementById('host1-port-author').disabled = !this.checked;
	document.getElementById('host1-port-publish').disabled = !this.checked;
    document.getElementById('host1-port-author').value = '';
	document.getElementById('host1-port-publish').value = '';	
	document.getElementById('host1-extra-str').disabled = this.checked;
	document.getElementById('host1-extra-str').value = '';
	document.getElementById('host1-extra-option-a').disabled = this.checked;
	document.getElementById('host1-extra-option-a').checked = false;
	document.getElementById('host1-extra-option-p').disabled = this.checked;
	document.getElementById('host1-extra-option-p').checked = false;	
	
};

document.getElementById('host2-option').onchange = function() {
    document.getElementById('host2-port-author').disabled = !this.checked;
	document.getElementById('host2-port-publish').disabled = !this.checked;	
    document.getElementById('host2-port-author').value = '';
	document.getElementById('host2-port-publish').value = '';	
	document.getElementById('host2-extra-str').disabled = this.checked;
	document.getElementById('host2-extra-str').value = '';
	document.getElementById('host2-extra-option-a').disabled = this.checked;
	document.getElementById('host2-extra-option-a').checked = false;
	document.getElementById('host2-extra-option-p').disabled = this.checked;
	document.getElementById('host2-extra-option-p').checked = false;		
};

document.getElementById('host3-option').onchange = function() {
    document.getElementById('host3-port-author').disabled = !this.checked;
	document.getElementById('host3-port-publish').disabled = !this.checked;
    document.getElementById('host3-port-author').value = '';
	document.getElementById('host3-port-publish').value = '';	
	document.getElementById('host3-extra-str').disabled = this.checked;
	document.getElementById('host3-extra-str').value = '';
	document.getElementById('host3-extra-option-a').disabled = this.checked;
	document.getElementById('host3-extra-option-a').checked = false;
	document.getElementById('host3-extra-option-p').disabled = this.checked;
	document.getElementById('host3-extra-option-p').checked = false;		
};