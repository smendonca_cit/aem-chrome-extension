function checkForValidUrl(tabId, changeInfo, tab) {

	chrome.pageAction.hide(tabId);	
	chrome.storage.sync.get({    
  
	  host1_text: '',
	  host2_text: '',
	  host3_text: '',  
	  host1_port_author: '',
	  host1_port_publish: '',  
	  host2_port_author: '',
	  host2_port_publish: '',
	  host3_port_author: '',
	  host3_port_publish: '',
	  host1_option: '',
	  host2_option: '',
	  host3_option: '',
	  host1_extra_option_a: '',
	  host2_extra_option_a: '',  
	  host3_extra_option_a: '',  
	  host1_extra_option_p: '',  
	  host2_extra_option_p: '',  
	  host3_extra_option_p: '',  
	  host1_extra_str: '',
	  host2_extra_str: '',
	  host3_extra_str: ''	  
	  
	  }, function(items) {
	 
	  
		if(items.host1_text != '' && tab.url.indexOf(items.host1_text) > -1) {
		
					
			if(items.host1_option == true && tab.url.indexOf(items.host1_port_author) > -1) {
			
				chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});			
				chrome.pageAction.show(tabId);
			}
			else if(items.host1_option == true && tab.url.indexOf(items.host1_port_publish) > -1) {
			
				chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_author.png'});			
				chrome.pageAction.show(tabId);
			}
			else if(items.host1_option == false) {
			
				chrome.pageAction.show(tabId);
				
				if(items.host1_extra_option_a == true && tab.url.indexOf(items.host1_extra_str) != -1) {
				
					chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});					
				}
				else if(items.host1_extra_option_p == true && tab.url.indexOf(items.host1_extra_str) == -1) {
				
					chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});
				}
			}
		}
		else if(items.host2_text != '' && tab.url.indexOf(items.host2_text) > -1) {
					
			if(items.host2_option == true && tab.url.indexOf(items.host2_port_author) > -1) {
			
				chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});			
				chrome.pageAction.show(tabId);
			}
			else if(items.host2_option == true && tab.url.indexOf(items.host2_port_publish) > -1) {
			
				chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_author.png'});			
				chrome.pageAction.show(tabId);
			}		
			else if(items.host2_option == false) {
			
				chrome.pageAction.show(tabId);
				
				if(items.host2_extra_option_a == true && tab.url.indexOf(items.host2_extra_str) != -1) {
				
					chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});					
				}
				else if(items.host2_extra_option_p == true && tab.url.indexOf(items.host2_extra_str) == -1) {
				
					chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});
				}
			}			
		}
		else if(items.host3_text != '' && tab.url.indexOf(items.host3_text) > -1) {
			
			if(items.host3_option == true && tab.url.indexOf(items.host3_port_author) > -1) {
			
				chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});			
				chrome.pageAction.show(tabId);
			}
			else if(items.host3_option == true && tab.url.indexOf(items.host3_port_publish) > -1) {
			
				chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_author.png'});			
				chrome.pageAction.show(tabId);
			}	
			else if(items.host3_option == false) {
								
				chrome.pageAction.show(tabId);
				
				if(items.host3_extra_option_a == true && tab.url.indexOf(items.host3_extra_str) != -1) {
				
					chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});					
				}
				else if(items.host3_extra_option_p == true && tab.url.indexOf(items.host3_extra_str) == -1) {
				
					chrome.pageAction.setIcon({tabId: tab.id, path: 'icon_publish.png'});
				}
			}			
		}
		
	 });
};

chrome.tabs.onUpdated.addListener(checkForValidUrl);

chrome.pageAction.onClicked.addListener(
	function(tab){
	
		chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
			var url = tabs[0].url;
			var newURL = null;
	
			chrome.storage.sync.get({    
		  
			  host1_text: '',
			  host2_text: '',
			  host3_text: '',  
			  host1_port_author: '',
			  host1_port_publish: '',  
			  host2_port_author: '',
			  host2_port_publish: '',
			  host3_port_author: '',
			  host3_port_publish: '',
			  host1_option: '',
			  host2_option: '',
			  host3_option: '',
			  host1_extra_option_a: '',
			  host2_extra_option_a: '',  
			  host3_extra_option_a: '',  
			  host1_extra_option_p: '',  
			  host2_extra_option_p: '',  
			  host3_extra_option_p: '',  
			  host1_extra_str: '',
			  host2_extra_str: '',
			  host3_extra_str: ''			  
			  
			  }, function(items) {
			  
				if(url.indexOf(items.host1_text) > -1) {
				
					if(items.host1_option == true) {
						
						if(url.indexOf(items.host1_port_author) > -1) {
							newURL = url.replace(items.host1_port_author, items.host1_port_publish);
						}
						else if(url.indexOf(items.host1_port_publish) > -1) {
							newURL = url.replace(items.host1_port_publish, items.host1_port_author);
						}
					
					}
					else {
								
						if(items.host1_extra_option_a == true) {
						
							if(tab.url.indexOf(items.host1_extra_str) != -1) {
								newURL = url.replace(items.host1_extra_str, '');
								newURL = newURL.replace('\/cf#','');								
							}
							else {
								newURL = 'http://' + items.host1_extra_str + url.substring(7,url.length);
							}						
						}
						else if(items.host1_extra_option_p == true) {
						
							if(tab.url.indexOf(items.host1_extra_str) != -1) {
								newURL = url.replace(items.host1_extra_str, '');
							}
							else {
								newURL = 'http://' + items.host1_extra_str + url.substring(7,url.length);
							}	
						}
						
					}					
				
				}
				else if(url.indexOf(items.host2_text) > -1) {
				
					if(items.host2_option == true) {
						
						if(url.indexOf(items.host2_port_author) > -1) {
							newURL = url.replace(items.host2_port_author, items.host2_port_publish);
						}
						else if(url.indexOf(items.host2_port_publish) > -1) {
							newURL = url.replace(items.host2_port_publish, items.host2_port_author);
						}
					
					}
					else {
					
						if(items.host2_extra_option_a == true) {
						
							if(tab.url.indexOf(items.host2_extra_str) != -1) {
								newURL = url.replace(items.host2_extra_str, '');
								newURL = newURL.replace('\/cf#','');								
							}
							else {
								newURL = 'http://' + items.host2_extra_str + url.substring(7,url.length);
							}						
						}
						else if(items.host2_extra_option_p == true) {
						
							if(tab.url.indexOf(items.host2_extra_str) != -1) {
								newURL = url.replace(items.host2_extra_str, '');
							}
							else {
								newURL = 'http://' + items.host2_extra_str + url.substring(7,url.length);
							}	
						}
						
					}					
				
				}
				else if(url.indexOf(items.host3_text) > -1) {
				
					if(items.host3_option == true) {
						
						if(url.indexOf(items.host3_port_author) > -1) {
							newURL = url.replace(items.host3_port_author, items.host3_port_publish);
						}
						else if(url.indexOf(items.host3_port_publish) > -1) {
							newURL = url.replace(items.host3_port_publish, items.host3_port_author);
						}
					
					}
					else {
					
						if(items.host3_extra_option_a == true) {
						
							if(tab.url.indexOf(items.host3_extra_str) != -1) {
								newURL = url.replace(items.host3_extra_str, '');
								newURL = newURL.replace('\/cf#','');								
							}
							else {
								newURL = 'http://' + items.host3_extra_str + url.substring(7,url.length);
							}						
						}
						else if(items.host3_extra_option_p == true) {
						
							if(tab.url.indexOf(items.host3_extra_str) != -1) {
								newURL = url.replace(items.host3_extra_str, '');
							}
							else {
								newURL = 'http://' + items.host3_extra_str + url.substring(7,url.length);
							}	
						}
						
					}					
				
				}
			
				chrome.tabs.create({ url: newURL });
				
			 });
	
			
		});		
	}
);

function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}